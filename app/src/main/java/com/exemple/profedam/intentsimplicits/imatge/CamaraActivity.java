package com.exemple.profedam.intentsimplicits.imatge;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.exemple.profedam.intentsimplicits.R;

public class CamaraActivity extends AppCompatActivity {

    ImageView imgFoto;
    private final int REQUEST_IMAGE_CAPTURE = 1000;
    private final int REQUEST_FILE_WRITER = 1001;
    private static  Uri locationForPhotos = Uri.parse("test");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);
    }

    public void onClick(View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            capturePhoto();

        } else {
                   /* A partir de la versió Android 6.0 hem de sol.licitar
            els permisos en temps d'execució */

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {



                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_FILE_WRITER);
            } else {
                capturePhoto();
            }
        }
        capturePhoto();

    }




    public void capturePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

        }
    }

        public void addImageToGallery ( Bitmap bitmap,  Context context) {



           String savedImageURL = MediaStore.Images.Media.insertImage(
                    getContentResolver(),bitmap,"Foto"+System.currentTimeMillis(), "Foto"
            );


            Toast.makeText (this,savedImageURL, Toast.LENGTH_LONG ).show();
        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap thumbnail = data.getParcelableExtra("data");
            // Do other work with full size photo saved in locationForPhotos
            imgFoto = findViewById(R.id.imgFoto);
            imgFoto.setImageBitmap(thumbnail);
           addImageToGallery(thumbnail, this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == this.REQUEST_FILE_WRITER && grantResults.length == 1 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {capturePhoto();}
        else
        {
            Toast.makeText (this, "Adios, no vas a tomar fotos", Toast.LENGTH_LONG).show();
        }
    }
}
